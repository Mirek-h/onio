//ONIO
const mongoose = require("mongoose");   // přístup k DB
const Joi = require('joi');             // validace
const express = require('express');     // základní framework
const cors = require('cors');
const app = express();
app.use(express.json());

app.use(cors());

app.get('/', (req, res) => {
    res.send('ONIO test');   // při volání root domeny
});
app.get('/with-cors', cors(), (req, res, next) => {
    res.json({ msg: 'WHOAH with CORS it works! 🔝 🎉' })
});

app.listen(3000, () => console.log('Listening on port 3000...'));        //  server


// DB
mongoose
    .connect("mongodb://localhost:27017/contactsdb", { useNewUrlParser: true })
    .then(() => console.log("Connected to MongoDB!"))
    .catch(error => console.error("Could not connect to MongoDB... ", error));

const contactSchema = new mongoose.Schema({
    name: String,
    surname: String,
    email: String,
    phone: String,
    street: String,
    number: Number,
    city: String,
    psc: String
});

const Contact = mongoose.model("Contact", contactSchema);

//GET - čti z DB všechny záznamy
app.get('/api/contacts', (req, res) => {
    //res.setHeader('Access-Control-Allow-Origin', '*');
    Contact.find().where().then(contacts => { res.json(contacts) })
});



//GET čti dle ID
app.get('/api/contacts/:id', (req, res) => {
    //res.setHeader('Access-Control-Allow-Origin', '*');
    const id = String(req.params.id);
    Contact.findById(id, (err, result) => {
        if (err || !result) {
        res.status(404).send("Kontakt nebyl nalezen.");
    }
    else
        res.json(result);
    });
});

async function getContacts() {
    const contacts = await Contact.find();
    return contacts;
}


//POST - vytvoř
app.post('/api/contacts', (req, res) => {
    //res.setHeader('Access-Control-Allow-Origin', '*');
    const { error } = validateContact(req.body);
    if (error) {
        res.status(400).send(error.details[0].message);
    } else {
        Contact.create(req.body)
            .then(result => { res.json(result) })
            .catch(err => { res.send("Nepodařilo se uložit kontakt!") });
    }
});


//validace vstupu
function validateContact(contact) {
    const schema = {
        name:           Joi.string().min(3).required(),
        surname:        Joi.string().min(3).required(),
        email:          Joi.string().required(),
        phone:          Joi.string().required(),
        street:         Joi.string().required(),
        number:         Joi.number().required(),
        city:           Joi.string().required(),
        psc:            Joi.string().min(5).required()
    };
    return Joi.validate(contact, schema);
}


//PUT - edituj
app.put('/api/contacts/:id', (req, res) => {
    //res.setHeader('Access-Control-Allow-Origin', '*');
    const { error } = validateContact(req.body, false);
    if (error) {
        res.status(400).send(error.details[0].message);
    } else {
        Contact.findByIdAndUpdate(req.params.id, req.body)
            .then(result => { res.json(result) })
            .catch(err => { res.send("Nepodařilo se uložit kontakt!") });
    }
});


// //DELETE - smaž
app.delete('/api/contacts/:id', (req, res) => {
    //res.setHeader('Access-Control-Allow-Origin', '*');
    Contact.findByIdAndDelete(req.params.id)
        .then(result => {
            if (result)
                res.json(result);
            else
                res.status(404).send("Kontakt s daným id nebyl nalezen!");
        })
        .catch(err => { res.send("Chyba při mazání kontaktu!") });
});